#!/bin/bash                                                                                                                                     

# Erfordert unter /var/log/ die Datei watcher.log
# Prüft ob der Prozess proftpd läuft, wenn nicht, 
# wird er als Service gestartet.
# Es empfiehlt sich, dass ein Cron Job kritische 
# Prozesse prüft und protokolliert. Ich erzeuge
# auf jeden Fall für jeden Zeitpunkt einen
# Eintrag im Logfile...

logfile=/var/log/watcher.log
x=`pidof proftpd`;

if [ "$x" == "" ]; then
       echo "$(date): ProFTPD aus - Neustart des Dienstes" >>$logfile
       service proftpd start
       else
       echo "$(date): ProFTPD ein - (Prozess-ID: $x)">>$logfile
fi

exit 0